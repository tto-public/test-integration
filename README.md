# Test Integration

[Lien maquette XD](https://xd.adobe.com/view/600e3b68-6647-4b85-b543-fbf19a07da8a-355d/specs/)

## Colors

```
'cod-gray': '#070707'
'cod-gray-light': '#1d1d1d'
'persimmon': '#fc654f'
'persimmon-hover': '#ed806f'
'concrete': '#f2f2f2'
'silver': '#cccccc'
'dove-gray': '#707070'
'tundora': '#4e4e4e'
```
